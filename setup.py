#!/usr/bin/env python
# coding: utf-8

from setuptools import setup, find_packages

requirements = [
        "numpy>=1.15.4",
        "opencv-python>=3.4.1.18",
        "pandas>=0.23.4",
        "scipy>=1.1.0",
        "matplotlib>=2.2.2",
        "scikit-image>=0.14.1",
        "scikit-learn>=0.20.1"
        "tensorflow>=1.12.0"
        "pytesseract>=0.2.5"
        "Keras>=2.2.4"
        "PyYAML>=3.12"
]

setup(name='zeamage',
      version='0.9',
      description='Maize computer vision toolkit',
      author='Jérôme Dury',
      author_email='jerome.dury@flyingsheep.fr',
      url='https://framagit.org/zeamage/zeamage',
      license='MIT',
      py_modules=['zeamage'],
      install_requires=requirements,
)
