History
=======

0.9. (2018-08-08)
------------------

- Clean documentation
- Computed average ear representation
- Multiple improvements on detection algorithms
- Clean method to limit memory used by temporaly stored image
- Count rows and kernels in a row numbers
- Gap detection on ear item
- Measurements with reference object
