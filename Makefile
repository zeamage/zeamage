include docs/Makefile

build_docs:
	cd docs/ && make html

clean_docs:
	cd docs/ && make clean
