Zeamage
===

Zeamage is an automated maize phenotyping Python library.


- [Description](#description)
- [Usage](#usage)
- [Images](#images)
- [Results](#results)
- [Installation](#installation)
- [Configuration](#configuration)



## Description

Zeamage is a free software, licensed under MIT license.

Ear and kernel attributes are important crop varieties traits to know.
They are valuable information and can greatly improve the process of crop selection made by farmers.
These attributes are however very time consuming to measure.

The library was developed to determine maize ear phenotype from a picture.
It allows to analyse and retrieve ears characteristics from a large amount of images.\
Otherwise it is possible to do real-time analysis with an acquisition software like [zeactl](https://framagit.org/zeamage/zeactl).


______
Currently determined characteristics are:

- ear length and width at differents specified quartiles
- mean kernels number in a row
- estimated number of rows

[Zeamage History file](https://framagit.org/zeamage/zeamage/blob/master/HISTORY.md)

## Usage

Please note that defaults settings have been set and optimized for standards 1280x720 maize pictures.

A classic pattern of the library use is :
```python
from zeamage import Zeamage

zea = Zeamage()
zea.detect_items(maize_image)
zea.measure_all()
zea.count_kernels()
zea.clean()
print(zea.ears[-1].results)
```
Access [full documentation](https://zeamage.frama.io/zeamage)

## Images

Images below represents the key steps of a classic Zeamage usage.

### Items segmentation
  <img src="images/segmentation.jpg" width="90%" />

______
### Ear computed dimensions
  <img src="images/measures.jpg" width="60%"/>

```
length: 124.6 mm
10% width: 30.4 mm
30% width: 32.3 mm
middle with: 31.4 mm
```

______
### Gaps and populated zones detection
  <img src="images/gaps.png" width="60%" />

```
populated length: 96.0 mm
```

______
### Kernel mean width/height determination
X-axis kernel width determination
  <p>
  <img src="images/kernels_segments.png" width="50%" />
  <img src="images/fft_kernels.png" width="49%" />
  </p>
Y-axis kernel height determination
  <p>
  <img src="images/rows_segments.png" width="50%" />
  <img src="images/fft_rows.png" width="49%" />
  </p>

______
### Computed image from extracted data
  <img src="images/computed.png" width="75%" />

## Results

Using a 1200 images database from about sixty maize varieties, the library accuracy was determined comparing results with manual measurements:
  <table>
  <tr><th>Measure</th><th>Fit r²</th><th>Fit RMSE</th></tr>
  <tr><td>ear length</td><td>0.97</td><td>5.50 mm</td></tr>
  <tr><td>ear kernels in a row</td><td>0.86</td><td>3.01 kernel</td></tr>
  <tr><td>ear rows number</td><td>0.71</td><td>1.21 row</td></tr>
  </table>


## Installation

Zeamage has currently only been tested in a Linux environment with Python 3.

To install a specific version of Zeamage, clone corresponding repository then :
```
$ cd zeamage
# pip install .
```

[Dependencies](https://framagit.org/zeamage/zeamage/blob/master/requirements.txt)

## Configuration
[Zeamage validate](https://framagit.org/zeamage/zeamage_validate) contains tools which may help configure Zeamage.
