.. zeamage documentation documentation master file, created by
   sphinx-quickstart on Thu Aug  9 16:37:33 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Zeamage's documentation!
=================================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   README.md
   context.rst
   zeamage.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
