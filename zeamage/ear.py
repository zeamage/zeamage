import logging
import sys
from warnings import warn
from pathlib import Path
import numpy as np
import uuid
import time
import cv2
from scipy import signal
from zeamage.item import Item
from zeamage.processing import (basicp, color, contour, display, edge, noise,
                                segmentp, shape, segmentation)
from zeamage.processing.feature import patch_to_features
from zeamage.processing.classifier import SvmClassifierCV, CnnClassifer

# from zeamage.settings import kernel_count as count_param
# from zeamage.settings import row_count as row_param

import skimage as skim
import matplotlib.pyplot as plt

logger = logging.getLogger(__name__)


class Ear(Item):
    """ Ear item.

    Ear carries measure algorithms methods and two important attributes:
        measurements (dict): measures completed at differents steps in pixels
        results (dict): All exploitable determined datas, most in mm
    """

    def __init__(self, img, ctr, number):
        # smooth contours
        # ctr = contour.smooth_contour(ctr, img, 0.01)
        super().__init__(img, ctr, number)
        self.preprocess()

        self.shape = img.shape
        self.masked_img = None

        # describe
        self.skel = None
        self.grain_mask = None
        self.cob_mask = None
        self.grain_zone_msk = np.array([])

        # measure
        self.measured = False

        # detect_gaps
        self.clahe = None

        self.kernel_counted = False
        self.row_counted = False
        self.lacunous = False
        self.kernels = 0
        self.quartile_segs = None

    def preprocess(self):
        """Reprocess contour image for the specific case of ear."""
        # frame = self.img.copy()
        # blurred = noise.blur(frame, 7)
        # hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
        # display.show_img(hsv, "reprocess")
        # lower_red = np.array([0,0,0])
        # upper_red = np.array([255,255,255])
        # mask = cv2.inRange(hsv, lower_red, upper_red)
        # res = cv2.bitwise_and(frame,frame, mask= mask)
        # edges = cv2.Canny(frame,100,200)
        # highlighted = noise.dilate_erode(edges,1,2)
        # display.show_img(highlighted, "reprocess")
        pass

    def describe(self, cfg):
        """Short summary.

        Args:
            cfg (type): Description of parameter `cfg`.

        Returns:
            type: Description of returned object.

        """
        disp = cfg["display"]

        # Get skeleton and rotate
        self.skel = self.skeleton(cfg)
        if disp["skeleton"]:
            display.show_img(self.skel, "skeleton")
        self.img_orientation()

        # Find sub components of the items
        self.segment_grain_cob(cfg)
        # self.segment_grain_cob2(cfg)

        # Update image summary
        self.img_summary = self.draw_grain_cob(self.img_summary.copy())

    def measure(self, cfg):
        """ Measure dimensions of the ear, corntip, area without grain
        """
        disp = cfg["display"]
        cfg = cfg["ear"]

        # Keep rotated skeleton points coordinates
        skel_coord = self.skel.nonzero()
        # Find polynomial estimation of ear kernel curve
        self.poly = segmentp.skel_to_polynom(skel_coord[1], skel_coord[0],
                                             cfg['poly_estimation_deg'])

        # Determine skeleton limits and quartiles measures
        self.segments = segmentp.determine_dimensions(
            self.poly, self.msk, cfg['poly_to_segments'], cfg['quartiles'])
        self.measures = {k: v["measure"] for k, v in self.segments.items()}

        # lacunary zone
        lacune_percent = self.lacunary()
        self.results['lacune_percent'] = lacune_percent

        # Count grains number on line
        if lacune_percent < cfg['lacune_treshold']:
            logger.info("Count grain on line")
            self.kernel_measure_count(cfg, disp)
            self.kernel_counted = True
        else:
            logger.info("To much lacune to count grain")

        # update img_summary with segment
        self.img_summary = self.draw_segments(img=self.img_summary)
        self.measured = True


    def skeleton(self, cfg):
        eroded_msk = shape.erode(self.msk.copy(), cfg["skeleton_erode_factor"])
        return shape.skeleton_line(eroded_msk, 4)


    def img_orientation(self):
        # Determinate skeleton orientation from points coordinates
        angle = segmentp.estimate_skel_angle(self.skel.nonzero())
        # Rotate everything still needed to horizontal
        angle_90 = np.sign(angle) * (90 - abs(angle))

        self.skel = basicp.rotate(self.skel, angle_90)
        self.img = basicp.rotate(self.img, angle_90)
        self.img_summary = basicp.rotate(self.img_summary, angle_90)
        self.msk = basicp.rotate(self.msk, angle_90)


    def segment_grain_cob(self, cfg):
        """
            Segment and label grain vs.cob area
            http://scikit-image.org/docs/stable/auto_examples/segmentation/plot_rag_merge.html#sphx-glr-auto-examples-segmentation-plot-rag-merge-py
            see: https://vcansimplify.wordpress.com/2014/07/06/scikit-image-rag-introduction/
        """
        disp = cfg["display"]
        cfg = cfg["segment_grain_cob"]

        self.cob_mask = None
        img = self.img_msk_bounding.copy()
        msk = self.msk_bounding.copy()

        # add black border to avoid side effect
        win = 20
        img = cv2.copyMakeBorder(
            img, win, win, win, win, cv2.BORDER_CONSTANT, value=(0, 0, 0))
        msk = cv2.copyMakeBorder(
            msk, win, win, win, win, cv2.BORDER_CONSTANT, value=(0, 0, 0))

        # get classifier
        # classifier = SvmClassifierCV(fpath=cfg["svm_grain-cob"])
        classifier = CnnClassifer(fpath=cfg["cnn_grain-cob"])

        #  Superpixel algo
        MERGED_FLAG = False
        sgt = segmentation.segment_mean_color(
            img,
            cfg["compactness"],
            cfg["num_segment"],
            cfg["agg_threshold"],
            merge=MERGED_FLAG)


        # assign a label to each segment
        labels = np.zeros(img.shape[:2], np.uint8)
        for s in np.unique(sgt):
            # construct a mask for the segment so we can compute image statistics for *only* the masked region
            sgt_mask = np.zeros(img.shape[:2], np.float)
            sgt_mask[sgt == s] = 1

            # get label for the segment
            # Assign background label
            intersect = np.sum(np.logical_and(msk == 0, sgt_mask == 1))
            if intersect > 0:
                labels[sgt == s] = 2
            else:
                # crop bounding seg and make a mask
                i, j = np.where(sgt_mask == 1)
                indices = np.meshgrid(
                    np.arange(min(i), max(i)),
                    np.arange(min(j), max(j)),
                    indexing='ij')

                patch = img[tuple(indices)]
                patch_img = patch.copy()
                # patch_mask = sgt_mask[tuple(indices)]

                patch =  cv2.resize(patch, (32, 32))
                patch = np.array([patch], dtype="float") / 255.0
                patch = np.expand_dims(patch[0], axis = 0)
                res = classifier.predict(patch)
                pred = int(round(res[0][0]))
                labels[sgt == s] = pred

                # --------------------------
                if cfg["export_img_patch"] == True:
                    key = str(pred)
                    if key in self.img_patch["without_msk"].keys():
                        self.img_patch["without_msk"][key].append(patch_img)
                    else:
                        self.img_patch["without_msk"][key] = [patch_img]

        # show the output of SLIC
        # out = cv2.cvtColor(img, cv2.COLOR_BRG2RGB)
        x, y, w, h = self.msk_bounding_indice()
        # img_sgt = skim.segmentation.mark_boundaries(img.copy(), sgt, (5, 5, 5)))
        slic_mask = skim.segmentation.find_boundaries(
            sgt, mode='thick').astype(np.float)
        slic_mask = slic_mask[win:win + h, win:win + w]
        self.slic_mask = self.bounding_to_mask(condition=slic_mask == 1)

        # apply bounding mask to all image
        labels = labels[win:win + h, win:win + w]
        self.grain_mask = self.bounding_to_mask(condition=labels == 1)
        self.cob_mask = self.bounding_to_mask(condition=labels == 0)

        if disp["label_grain_cob"]:
            display.show_img(self.draw_grain_cob(), "label_grain_cob")


    def segment_grain_cob2(self, cfg):
        """ Determine unpopulated areas of the ear.

        Ear must have been measured to proceed.
        Algorithm use both coloration and roughness detection.

        Returns:
            Mask representing all gaps found over the ear
        """
        disp = cfg["display"]
        cfg = cfg["segment_grain_cob2"]
        # if not self.measured:
        #     raise Exception('Ear not measured')

        # Roughness detection with clahe and sobel algorithms
        clahe = edge.clahe(self.img_msk, cfg['clahe_gridsize'])
        sobel = edge.sobel_colored(clahe, cfg['sobel'])
        # Color variance between differents image channels
        c_var = color.color_var(self.img_msk)
        # Keep values around 30/255, mostly representing gaps
        c_var = np.abs(c_var.astype(float) - 30)
        c_var = (255 - c_var)**2
        c_var = 255 * c_var / np.max(c_var)
        c_var = c_var.astype(np.uint8)
        # Merge blured color and roughness informations
        proportion = cfg['balance']
        sobel = noise.blur(sobel, cfg['blur']) * proportion[0]
        c_var = noise.blur(c_var, cfg['blur']) * proportion[1]
        gap_info = ((c_var + sobel) / sum(proportion)).astype(np.uint8)
        # Smooth before treshold
        gap_info = noise.blur(gap_info, cfg['blur'])
        # Keep interesting found gaps
        gap_mask = basicp.binary(gap_info, cfg['g_tresh'])
        gap_mask = contour.keep(gap_mask, cfg['max_gaps'],
                                     cfg['min_area'])
        display.show_img(gap_mask)

        # apply bounding mask to all image
        # labels = labels[win:win + h, win:win + w]
        # self.grain_mask = self.bounding_to_mask(condition=labels == 1)
        self.cob_mask2 = gap_mask

        if disp["label_grain_cob2"]:
            display.show_img(self.draw_grain_cob2(), "label_grain_cob2")


    def lacunary(self):
        return basicp.mask_diff(self.cob_mask, self.msk) * 100


    def detect_grain_zone(self, cfg, disp):
        """ Estimate cob zones among gaps.

        Gaps detection is needed before calling this method.

        """
        # Gaps distribution over x-axis
        dist = np.sum(self.cob_mask, axis=0) / 255
        y1, y2, x1, x2 = basicp.mask_shape(self.msk)
        dist = dist[x1:x2]
        # Smooth distribution
        dist = contour.smooth(dist, cfg['dist_blur'])

        # Add mininimal cob detection len
        dist[:10] = 1
        dist[-10:] = 1
        # Limit search to the first populated zones
        zeros = np.where(dist == 0)[0]
        if not zeros.any():
            warn('Could not search for extremum cobs')
            return np.array([])
        # Calculate gradient of the distribution
        grad = np.gradient(dist)
        # Search zones corresponding to cob parts
        cx1 = x1 + np.argmin(grad[: zeros[0]]) if \
            grad[:zeros[0]].any() else x1
        cx2 = x2 - len(grad[zeros[-1]:]) + np.argmax(grad[zeros[-1]:]) if \
            len(grad[zeros[-1]:]) > 1 else x2
        # Store results and return mask
        self.measures['L_with_cob'] = cx2 - cx1
        if (cx2 - cx1) > cfg['min_seg_len']:
            res = np.zeros((self.msk.shape), np.uint8)
            res[:, cx1:cx2] = 255
        else:
            res = np.array([])

        self.grain_zone_msk = res


    def kernel_measure_count(self,cfg, disp):
        """ Measure the  width of kernel and count the number in a row, according to ear skeleton.

        The method use x-axis FFT peak detection on clahe ear image.

        Args:
            disp_segs (bool): Compute and display segments used during FFT
        Returns:
            kernel width
        """
        self.detect_grain_zone(cfg, disp)
        img_clahe = edge.clahe(self.img_msk, cfg['clahe_gridsize'])
        if not self.grain_zone_msk.any():
            logger.info('Cobs parts hasn\'t been determined')
            return
        # Merge ear mask and cob mask
        tmask = self.msk & self.grain_zone_msk
        y1, y2, x1, x2 = basicp.mask_shape(tmask)
        base = self.poly(range(x1, x2))
        # Create all the correct segments to apply FFT following ear skeleton
        segs = segmentp.populate_segments(
            base,
            cfg['inter_seg'],
            cfg['min_seg_len'],
            np.invert(np.invert(tmask) | self.cob_mask),
            (y1, y2, x1, x2), )
        if not segs:
            raise Exception('Nowhere to apply horizontal FFT')
        # Apply clahe multiple times to reduce low frequencies strength
        gray = edge.clahe(self.img_msk, cfg['clahe'])
        for i in range(cfg['clahe_recursion']):
            gray = edge.clahe(gray)

        gray = basicp.gray(gray)
        gray = noise.blur(gray, cfg['clahe_blur'], 2)
        # Calculate FFT for every segments
        all_freqs = []
        for s in segs:
            vals = [int(gray[y, s[0] + x]) for x, y in enumerate(s[1:])]
            # Detrend signal to avoid underfitting
            vals = signal.detrend(vals)
            all_freqs.append(
                edge.fft(vals, img_clahe.shape[1], cfg['fft_blur']))
        # Merge all frequency datas
        freqs = [int(sum(x)) for x in zip(*all_freqs)]
        f_axis = np.fft.fftfreq(len(freqs))
        # Find global peak location
        maxi = np.argmax(freqs)
        if f_axis[maxi] < 1 / cfg['max_px_len']:
            raise Exception('FFT detection returned wrong value')

        # Kernel width
        self.measures['kernel_width'] = round(1. / f_axis[maxi],2)
        # Calculate kernel length then kernels number in a row
        self.results['kernel_row_number'] = int(round((x2 - x1) / self.measures['kernel_width']))
        self.kernel_counted = True

        # Display segments if necessary
        if disp["fftseg"]:
            d = self.msk.copy()
            for s in segs:
                x = s[0]
                for p in s[1:]:
                    cv2.circle(d, (x, p), 1, (200, 100, 0), 1)
                    x += 1
            display.show_img(d, 'Segments used for FFT analysis')
        # display output
        if disp["fftplot"]:
            pos = int(len(freqs) / 2)
            title = "FFTplot"
            text = "kernel with: {0}\nkernel row number: {1}".format(
                    int(self.measures['kernel_width']),
                    int(self.results['kernel_row_number']))
            display.plot((f_axis[:-pos], freqs[:-pos]), title=title, text=text, vert=f_axis[maxi])


    #
    # def count_rows(self, disp_segs=False):
    #     """ Estimate number of rows in the ear.
    #
    #     The method use y-axis FFT peak detection on clahe ear image. Segments
    #     are taken around each quartiles and compared to relevant width.
    #
    #     Args:
    #         disp_segs (bool): Compute and display segments used during FFT
    #     Returns:
    #         A set of two arrays, containing y and x-axis FFT values
    #         The maximum frequency value, 1/kernel_width in pixels
    #     """
    #     # Merge ear mask and cob mask
    #     tmask = self.msk & self.cob_mask
    #     x1, x2, y1, y2 = basicp.mask_shape(tmask)
    #     segs_list = []
    #     # Determine segments to use for each previously measured quartile
    #     for pts in self.quartile_segs[0]:
    #         dy = pts[1][1] - pts[0][1]
    #         dx = pts[1][0] - pts[0][0]
    #         x_mean = int((pts[1][0] + pts[0][0]) / 2)
    #         # Determine center vertical segment
    #         line = [x_mean + int((y / dy) * dx) for y in range(dy)]
    #         width = int((row_param['width']) * (y2 - y1) / 2)
    #         # Height/width limited zone to fill with other vertical segments
    #         zone = (x_mean - width, x_mean + width, pts[0][1], pts[1][1])
    #         segs_list.append(
    #             segmentp.populate_segments(
    #                 line,
    #                 row_param['inter_seg'],
    #                 row_param['min_seg_len'],
    #                 np.invert(np.invert(tmask) | self.gap_mask),
    #                 zone,
    #                 axis=True,
    #                 cut=1 - row_param['height'], ))
    #     if not np.array(segs_list).any():
    #         raise Exception('Nowhere to apply vertical FFT')
    #     # Display segments if necessary
    #     if disp_segs:
    #         d = self.masked_img.copy()
    #         for s in [s for segs in segs_list for s in segs]:
    #             y = s[0]
    #             for p in s[1:]:
    #                 cv2.circle(d, (p, y), 1, (200, 100, 0), 1)
    #                 y += 1
    #         display.show_img(d, 'Segments used for rows FFT analysis')
    #     # Apply clahe and blur to enhance sinusoidal informations on signal
    #     gray = basicp.gray(edge.clahe(self.masked_img, row_param['clahe']))
    #     gray = noise.blur(gray, row_param['clahe_blur'], 1)
    #     # Calculate every segment FFT
    #     quartiles_measure = []
    #     quartiles_result = []
    #     # Use weights in order to merge quartiles frequency results fairly
    #     quartiles_weight = [
    #         np.sum([len(sg) for sg in sgs]) for sgs in segs_list
    #     ]
    #     freqs_50 = []
    #     f_axis_50 = []
    #     maxi_50 = 0
    #     pos_50 = 0
    #     # Calculate FFT for every segments in every quartiles
    #     for segs, quart in zip(segs_list, self.quartile_segs[1]):
    #         all_freqs = []
    #         for s in segs:
    #             vals = [int(gray[s[0] + y, x]) for y, x in enumerate(s[1:])]
    #             # Detrend signal to avoid underfitting
    #             vals = signal.detrend(vals)
    #             all_freqs.append(
    #                 edge.fft(vals, self.clahe.shape[0], row_param['fft_blur']))
    #         if not all_freqs:
    #             quartiles_weight[len(quartiles_measure)] = 0
    #             quartiles_measure.append(0)
    #             quartiles_result.append(0)
    #             continue
    #         # Merge all frequency datas in current quartile
    #         freqs = [int(sum(x)) for x in zip(*all_freqs)]
    #         f_axis = np.fft.fftfreq(len(freqs))
    #         maxi = np.argmax(freqs)
    #         # Display purpose only (keep value at ear center)
    #         if quart == 50:
    #             freqs_50 = freqs
    #             f_axis_50 = f_axis
    #             maxi_50 = maxi
    #             pos_50 = int(len(freqs) / 2)
    #         # Find global peak location
    #         f_maxi = f_axis[maxi]
    #         quartiles_measure.append(1. / f_maxi)
    #         quartiles_result.append(
    #             np.pi *
    #             (self.measures['quartiles'][quart]) / quartiles_measure[-1])
    #     # Keep weighted mean of each quartile measures
    #     self.measures['kernel_height'] = np.average(
    #         quartiles_measure, weights=quartiles_weight)
    #     self.results['rows_nb'] = np.poly1d(row_param['row_correction'])(
    #         round(np.average(quartiles_result, weights=quartiles_weight), 1))
    #     self.row_counted = True
    #     if not freqs_50:
    #         return None
    #     return True, (f_axis_50[:-pos_50], freqs_50[:-pos_50]), \
    #         f_axis_50[maxi_50]

    def draw_segments(self, img=None):
        if img is None:
            img = self.img.copy()
        segments = [s for v in self.segments.values() for s in v['segment']]
        return display.draw_lines(img, segments, basicp.COLORS['red'])

    def draw_cob_label(self, img=None):
        if img is None:
            img = self.img.copy()
        return display.draw_label(img, self.cob_mask, basicp.COLORS['blue'],
                                  0.3)

    def draw_grain_cob(self, img=None):
        if img is None:
            img = self.img.copy()
        img = cv2.bitwise_or(
            img,
            img,
            mask=cv2.bitwise_not(self.slic_mask))
        # add cob mask
        return self.draw_cob_label(img=img)

    def draw_grain_cob2(self, img=None):
        if img is None:
            img = self.img.copy()
        return display.draw_label(img, self.cob_mask2, basicp.COLORS['blue'],
                                  0.3)
