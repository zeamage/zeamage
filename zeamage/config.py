import os
import yaml


def setup_config(
    config={},
    env_key='ZEA_CFG'
):
    """Setup default configuration

    """
    path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.yaml')

    # override path by env variable
    value = os.getenv(env_key, None)
    if value:
        path = value

    if os.path.exists(path):
        with open(path, 'rt') as f:
            default_config = yaml.safe_load(f.read())
            default_config.update(config)
            config = default_config
    return config
