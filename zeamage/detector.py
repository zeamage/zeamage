import logging

import numpy as np
import cv2
from skimage.feature import peak_local_max
from skimage.morphology import watershed
from scipy import ndimage
from zeamage.processing import basicp, contour, edge, noise, display
from zeamage.ear import Ear
from zeamage.grain import Grain
from zeamage.item import Item
from zeamage.reference import Reference

logger = logging.getLogger(__name__)


def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect


def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0])**2) + ((br[1] - bl[1])**2))
    widthB = np.sqrt(((tr[0] - tl[0])**2) + ((tr[1] - tl[1])**2))
    maxWidth = max(int(widthA), int(widthB))

    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0])**2) + ((tr[1] - br[1])**2))
    heightB = np.sqrt(((tl[0] - bl[0])**2) + ((tl[1] - bl[1])**2))
    maxHeight = max(int(heightA), int(heightB))

    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array(
        [[0, 0], [maxWidth - 1, 0], [maxWidth - 1, maxHeight - 1],
         [0, maxHeight - 1]],
        dtype="float32")

    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    # return the warped image
    return warped


def detect_small_shapes(img, msk, cfg):
    k5 = np.ones((5,5),np.uint8)
    k3 = np.ones((3,3),np.uint8)
    # apply background mask

    # Tune color
    img_crop_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    # increase brigthness
    v = img_crop_hsv[:, :, 2]
    increase = 100
    v = np.where(v <= 255 - increase, v + increase, 255)
    img_crop_hsv[:, :, 2] = v
    # convert back to BRG
    img_crop_bright = cv2.cvtColor(img_crop_hsv, cv2.COLOR_HSV2BGR)

    # Increase contrast
    img_clahe = edge.clahe(img_crop_bright)

    # ------------------------
    # img_c = img_clahe.copy()
    # img_d = img_clahe.copy()
    # img_c[np.where(msk==0)] = 0
    #
    # # gray = cv2.cvtColor(img_c, cv2.COLOR_BGR2GRAY)
    # # Blur image to reduce noise
    # # Highlight edges
    # blurred = cv2.bilateralFilter(img, 11, 17, 17)
    # highlighted = edge.sobel_colored(blurred)
    # display.show_img(highlighted)
    # highlighted = cv2.Canny(highlighted,100,200)
    # display.show_img(highlighted)
    #
    # i, cnts, h = cv2.findContours(highlighted.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #
    # contours = []
    # for ctr in cnts:
    #     area = contour.check_area(ctr, img_d, 0.0001)
    #     if area is not None and contour.check_inner(ctr, img_d):
    #         # Store contours
    #         contours.append(ctr)
    #
    # cv2.drawContours(img_d, cnts, -1, (0, 255, 0), 1)
    # cv2.drawContours(img_d, contours, -1, (0,0, 255), 1)
    # display.show_img(img_d)

    # # Remove hairs loops and thinner parts
    # bin = cv2.threshold(highlighted, 0, 255,
    #                          cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    # display.show_img(bin)
    # # Fill detected shapes
    # binary_filled = contour.fill_holes(bin)
    # # binary_filled = cv2.erode(binary_filled, k3,iterations = 4)
    # display.show_img(binary_filled)
    # dist = cv2.distanceTransform(binary_filled, cv2.DIST_L2, 3)
    # cv2.normalize(dist, dist, 0, 1.0, cv2.NORM_MINMAX)
    # display.show_img(dist)
    # ------------------------

    # Reduce the color number
    img_clahe = edge.kmeans(img_clahe, K=2)
    # binarize
    gray = cv2.cvtColor(img_clahe, cv2.COLOR_BGR2GRAY)
    # gray = cv2.normalize(gray, gray, 0, 255, cv2.NORM_MINMAX)
    thresh = cv2.threshold(gray, 0, 255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1] # Otsu's thresholding
    # morphological transformation
    erosion = cv2.erode(thresh, k3,iterations = 8)
    fg_msk = cv2.dilate(erosion,k3,iterations = 6)

    # display.show_img(msk)
    # display.show_img(fg_msk)

    # compute the exact Euclidean distance from every binary
    # pixel to the nearest zero pixel, then find peaks in this
    # distance map
    msk = cv2.erode(thresh, k3,iterations = 1)
    D = ndimage.distance_transform_edt(fg_msk)
    localMax = peak_local_max(D, indices=False, min_distance=12, labels=fg_msk)

    # perform a connected component analysis on the local peaks,
    # using 8-connectivity, then appy the Watershed algorithm
    markers = ndimage.label(localMax, structure=np.ones((3, 3)))[0]
    labels = watershed(-D, markers, mask=msk)

    item_contours = []

    for label in np.unique(labels):
        # if the label is zero, we are examining the 'background'
        # so simply ignore it
        if label == 0:
          continue

        # otherwise, allocate memory for the label region and draw
        # it on the mask
        label_msk = np.zeros(img.shape[:2], dtype="uint8")
        label_msk[labels == label] = 255

        # detect contours in the mask and grab the largest one
        i, cnts, h = cv2.findContours(label_msk.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        c = max(cnts, key=cv2.contourArea)
        # if contour.check_inner(c, img):
        item_contours.append(c)

    return item_contours


def detect_main_shapes(img, cfg, msk=None, bin_treshold=None, sorted=False):
    """ Segment image in order to detect items shapes.

    Args:
        img (np.ndarray): Image to process
    Returns:
        Polygons arrays representing found items contours
    """
    # apply background mask
    if msk is not None:
        img[np.where(msk == 0)] = basicp.COLORS['black']

    logger.info("Contour detection: pre-processing image")
    # Blur image to reduce noise
    blurred = noise.blur(img, cfg['blur_factor'])
    # Highlight edges
    highlighted = edge.sobel_colored(blurred)
    # Remove hairs loops and thinner parts
    highlighted = noise.dilate_erode(highlighted,
                                     cfg['open_close'][0],
                                     cfg['open_close'][1])
    # Transform to binary image with a treshold
    binary_img = basicp.binary(highlighted, bin_treshold)
    # Fill detected shapes
    binary_filled = contour.fill_holes(binary_img)
    # Remove noise and hairs by a second dilate-erode process
    binary_img = noise.dilate_erode(binary_filled,
                                    cfg['shape_open_close'][0],
                                    cfg['shape_open_close'][1])

    # remove blue border
    kernel = np.ones((5,5), np.uint8)
    binary_im = cv2.erode(binary_img,kernel,iterations = 1)

    # Find relevant contours from binary img
    contours = contour.find_outermost(binary_im,
                                      cfg['min_detect_area'])

    # sorted contour from up to down and left to right
    if len(contours)>0:
        contours, _ = contour.sort_contours(contours)
    return contours


def detect_frame(img, cfg, bin_treshold=40):
    """Find the outermost frame contour. Return an approximation"""
    ctrs = detect_main_shapes(
        img, cfg, bin_treshold=bin_treshold, sorted=True)
    logger.info("{} frame detected".format(len(ctrs)))
    # check if frame contour exists
    if len(ctrs) == 0:
        logger.info("No contour frame detected, may be frame does not exist!")
        return None
    else:
        # Find the index of the largest contour
        areas = [cv2.contourArea(c) for c in ctrs]
        max_index = np.argmax(areas)
        ctr = ctrs[max_index]
        # check if rectanlge
        ctr_approx = cv2.approxPolyDP(ctr, 0.01 * cv2.arcLength(ctr, True),
                                      True)
    return ctr_approx


def detect_background(img, cfg, lower_bound=(100, 50, 20), upper_bound=(125, 255, 255)):
    """ Background detect ,based on hsv color"""
    # remove background color
    blur = cv2.blur(img,(5,5))
    hsv_im = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)
    hsv_msk = cv2.inRange(hsv_im,lower_bound, upper_bound)
    bg_msk = np.zeros(hsv_msk.shape, np.uint8)
    bg_msk[hsv_msk == 0] = 255
    k3 = np.ones((3,3),np.uint8)
    bg_msk = cv2.erode(bg_msk,k3,iterations = 3)
    bg_msk = cv2.dilate(bg_msk,k3,iterations = 2)

    return bg_msk


def detect_item(img, ctr, obj_type, number, max_approx=5, reference=True):
    """ Determine from a given contour which item the shape represents.

    The contour may be returned modified if necessary.

    Args:
        ctr (np.ndarray): Single contour to analyse
        height (int):     Mask desired height
        width (int):      Mask desired width
    Returns:
        Determined item instance
    """
    # max approx should be >= 5
    if max_approx < 5:
        max_approx = 5

    # Do an approximation of the shape
    approx = cv2.approxPolyDP(ctr, 0.01 * cv2.arcLength(ctr, True), True)
    # Reference objects should be rectangular
    if len(approx) in range(4, max_approx) and reference is True:
        item_name = 'reference'
        ini = 0.01
        while len(approx) > 4:
        	ini += 0.001
        	approx = cv2.approxPolyDP(ctr, ini*cv2.arcLength(ctr, True), True)
        ctr = approx
        return Reference(img, ctr, number)
    elif len(approx) > (max_approx-1):
        if obj_type == "ear":
            return Ear(img, ctr, number)
        if obj_type == "grain":
            return Grain(img, ctr, number)
        else:
            return Item(img, ctr, number)
    return None
