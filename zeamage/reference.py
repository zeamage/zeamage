import logging
from fractions import Fraction

import pytesseract

from zeamage.item import Item
from zeamage.processing import basicp, segmentp,  display

import cv2
import time

logger = logging.getLogger(__name__)


class Reference(Item):
    """ Reference item.

    Reference items should be rectangular objects present on image.
    Their known dimensions in mm make it possible to establish a relation
    in pixels.
    """

    def __init__(self, img, ctr, number):
        super().__init__(img, ctr, number)
        self.text = ""

    def describe(self, cfg):
        # TODO
        pass

    def read_text(self):
        # in order to apply Tesseract v4 to OCR text we must supply
        # (1) a language, (2) an OEM flag of 4, indicating that the we
        # wish to use the LSTM neural net model for OCR, and finally
	    # (3) an OEM value, in this case, 7 which implies that we are
	    # treating the ROI as a single line of text
        config = ("-psm 7")
        x,y,w,h = self.msk_bounding_indice(border=-20)
        roi = self.img.copy()[y:y+h, x:x+w]
        self.text = pytesseract.image_to_string(roi, config=config)
        logger.debug("Read text on reference: {}".format(self.text))


    def measure(self, width, height):
        """ Determine relation between millimeters and image pixels.

        Returns:
            New determined pixelsPerMetric
        """
        # Join each couple of points of countour in segments
        segments = [(self.ctr[p][0], self.ctr[(p + 1) % len(self.ctr)][0])
                    for p in range(len(self.ctr))]
        # Measure each four segments
        m_segs = [segmentp.measure_segment(s) for s in segments]
        # Get mean measures on each parallel couples
        dists = [(m_segs[0] + m_segs[2]) / 2, (m_segs[1] + m_segs[3]) / 2]

        # Detect if shapes are very differents from model given in parameters
        ratio_real = width / height
        pixel_width = max(dists)
        pixel_height = min(dists)
        ratio_calc = pixel_width / pixel_height

        if max(ratio_real, ratio_calc) / min(ratio_real, ratio_calc) > 1.5:
            logger.warning(
                'Reference object shape seems different from the '
                'parameters.\nPlease check ref_length > ref_width.'
                '\nRatio in image: {} | Ratio in parameters {}'.format(
                    Fraction(ratio_calc).limit_denominator(10),
                    Fraction(ratio_real).limit_denominator(10)))
            self.pixelsPerMetric = None
        else:
            # Determine new pixelsPerMetric and return it
            self.pixelsPerMetric = ((
                pixel_width / width + pixel_height / height) / 2 )
