from abc import ABC, abstractmethod
import collections
import numpy as np
import cv2
from zeamage.processing import basicp, contour, display


def map_nested_dicts(ob, func, suffix=""):
    if isinstance(ob, collections.Mapping):
        return {suffix + k: map_nested_dicts(v, func) for k, v in ob.items()}
    else:
        return func(ob)


class Item(ABC):
    """ Item abstract class.

    Item represent characterised shapes found on the image.
    """

    def __init__(self, img, ctr, number):
        """ Item should be initialized with at least those arguments.
        Args:
            img (np.ndarray): Base image where item is present
            ctr (np.ndarray): Contour of the item in image
            number (int):     Item id number on the image
        """
        self.img = img.copy()
        self.img_patch = {"with_msk":{}, "without_msk":{}} # Export Patch image to train classifier
        self.ctr = ctr
        self.img_summary = img.copy()
        self.number = number
        self.measures = {}  # measure in pixel
        self.measures_metrics = {}  # measure in metrics
        self.results = {}  # result that does not need conversion

        # mask
        self.msk = basicp.contour2mask(self.img, [ctr])

        # add contour mask
        if number != -1:
            display.draw_contours(self.img_summary, [ctr])

    def msk_bounding_indice(self, border=15):
        x,y,w,h = cv2.boundingRect(self.msk)
        return (x-border,y-border, w+border, h+border)

    @property
    def img_bounding_summary(self):
        x,y,w,h = self.msk_bounding_indice()
        return self.img_summary.copy()[y:y+h, x:x+w]

    @property
    def img_bounding(self):
        x,y,w,h = self.msk_bounding_indice()
        return self.img.copy()[y:y+h, x:x+w]

    @property
    def img_msk_bounding(self):
        img = cv2.bitwise_and(self.img,self.img,mask = self.msk)
        x,y,w,h  = self.msk_bounding_indice()
        return img.copy()[y:y+h, x:x+w]

    @property
    def msk_bounding(self):
        x,y,w,h = self.msk_bounding_indice()
        return self.msk[y:y+h, x:x+w]

    @abstractmethod
    def describe(self, cfg):
        """Describe item.

          Make mask for the different components/parts of the item
        """
        raise NotImplementedError('Describe not implemented')

    @abstractmethod
    def measure(self, cfg):
        """ Measure item.

        Measurement method may be different between each items. Measurements are in pixel
        """
        raise NotImplementedError('Measurement not implemented')

    @property
    def img_msk(self):
        """Image with mask."""
        img_msk = self.img.copy()
        img_msk[self.msk==0] = 0
        return img_msk

    def to_metrics(self, pixelsPerMetric, correction=1):
        """ Convert  pixel measure in metrics (mm)"""
        self.measures_metrics = map_nested_dicts(
            self.measures, lambda y: y / pixelsPerMetric / correction, suffix="metrics_")


    def bounding_to_mask(self, condition):
        bmask = np.zeros(self.img_bounding.shape[:2], np.uint8)
        bmask[condition] = 255

        mask = np.zeros(self.img.shape[:2], np.uint8)
        x,y,w,h = self.msk_bounding_indice()
        mask[y:y+h,x:x+w] = bmask
        return mask
