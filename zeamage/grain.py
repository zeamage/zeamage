import logging

from zeamage.item import Item

logger = logging.getLogger(__name__)


class Grain(Item):
    """ Ear item.

    Grain carries measure algorithms methods and two important attributes:
        measurements (dict): measures completed at differents steps in pixels
        results (dict): All exploitable determined datas, most in mm
    """

    def __init__(self, img, ctr, number):
        # smooth contours
        # ctr = contour.smooth_contour(ctr, img, 0.01)
        super().__init__(img, ctr, number)
        self.preprocess()

        self.shape = img.shape
        self.masked_img = None

        # describe
        # TODO

        # measure
        # TODO


    def preprocess(self):
        """Reprocess contour image for the specific case of ear."""
        # frame = self.img.copy()
        # blurred = noise.blur(frame, 7)
        # hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
        # display.show_img(hsv, "reprocess")
        # lower_red = np.array([0,0,0])
        # upper_red = np.array([255,255,255])
        # mask = cv2.inRange(hsv, lower_red, upper_red)
        # res = cv2.bitwise_and(frame,frame, mask= mask)
        # edges = cv2.Canny(frame,100,200)
        # highlighted = noise.dilate_erode(edges,1,2)
        # display.show_img(highlighted, "reprocess")
        pass

    def describe(self, cfg):
        """Short summary.

        Args:
            cfg (type): Description of parameter `cfg`.

        Returns:
            type: Description of returned object.

        """
        disp = cfg["display"]
        pass

    def measure(self, cfg):
        """ Measure dimensions of the ear, corntip, area without grain
        """
        disp = cfg["display"]
        cfg = cfg["grain"]
        pass

    # def draw_grain_contour(self, img=None):
    #     if img is None:
    #         img = self.img.copy()
    #     return display.draw_label(img, self.cob_mask, basicp.COLORS['blue'],
