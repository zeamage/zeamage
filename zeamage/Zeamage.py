import collections
import itertools
import logging
from warnings import warn

import numpy as np
import pandas as pd

import cv2
from zeamage import detector
from zeamage.config import setup_config
from zeamage.ear import Ear
from zeamage.grain import Grain
from zeamage.processing import basicp, display, noise, segmentp
from zeamage.reference import Reference

logger = logging.getLogger(__name__)


def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


class Zeamage():
    """ Zeamage main class.

    Once instanciated, this class could be used to successively
    analyse multiple maize pictures.

    We do recommend to use clean() method between each image
    analysis in order to avoid using too much memory.

    A classic pattern of use is :
        | zea = Zeamage()
        | zea.detect_items(maize_image)
        | zea.measure_all()
        | zea.count_kernels()
        | zea.clean()
        | print(zea.ears[-1].results)

    """

    def __init__(self, img, config={}):
        self.cfg = setup_config(config)
        # input image
        self.input_img = img.copy()
        self.img = img.copy()  # inner frame image if frame
        self.summary_img = None
        self.pixelsPerMetric = None
        # Objects found in the image
        self.references = []
        self.items = []

    def _items_by_cls(self, cls):
        """Filter item instance by class"""
        return [
            i for i, inst in enumerate(self.items) if isinstance(inst, cls)
        ]

    def _items_count_by_cls(self):
        def cls_name_func(i):
            return i.__class__.__name__

        data = sorted(self.items, key=cls_name_func)
        grp_items = itertools.groupby(data, cls_name_func)
        return {k: len(list(grp)) for k, grp in grp_items}

    @property
    def _id_reference(self):
        """ Build id from text on reference object. Usefull for future reuse of image.
        """
        id_ref = ""
        for r in self.references:
            id_ref = id_ref + r.text
        logger.debug("Full reference id: {}".format(id_ref))
        return id_ref

    def detect_items(self, obj_type="ear", number=0):
        """ Detect differents items from a given picture.

        Store newly detected ears in self.ears and new reference objects
        in self.references using given variety name and id number.

        Args:
            obj_type (np.ndarray): Type of items to bze detected
            number (int):     Id number of the first item
        """
        cfg = self.cfg['detect_items']
        disp = cfg['display']

        # -----
        self.correction = float(cfg["reference"]["correction"])
        ref_size = (int(cfg['reference']['width']),
                    int(cfg['reference']['height']))

        #-------
        # Find frame if exist
        if cfg["frame"]["with"] == True:
            # Get frame contours and make maks with it
            ctr_frame = detector.detect_frame(
                self.img, cfg, bin_treshold=30)  # contour frame
            if ctr_frame is not None:
                ctr_msk = basicp.contour2mask(
                    self.img, [ctr_frame])  # mask of the contour frame
                # kind of padding
                k3 = np.ones((3,3),np.uint8)
                ctr_msk = cv2.erode(ctr_msk,k3,iterations = 1)

                self.img[np.where(ctr_msk == 0)] = basicp.COLORS[
                    'black']  # apply mask to img

                # crop & rotate image
                self.img = basicp.crop_rectangle(
                    self.img, ctr_frame, padding=0)
                ctr_msk = basicp.crop_rectangle(
                    ctr_msk, ctr_frame, padding=0)
                if disp['orig_frame_less']:
                    display.show_img(self.img.copy(), "Original without frame")

                # store frame as reference object
                if cfg["frame"]["width"] & cfg["frame"]["height"]:
                    item_frame = Reference(self.img, ctr_frame, -1)
                    item_frame.measure(
                        int(cfg["frame"]["width"]),
                        int(cfg["frame"]["height"]))
                    self.references.append(item_frame)

        # ------
        # Find background
        bg_msk = detector.detect_background(self.img, cfg)
        if cfg["frame"]["with"] == True and ctr_frame is not None:
            bg_msk[ctr_msk==0] = 0 # add real controu mask

        # ------
        # Find contour
        if obj_type == "ear":
            # apply padding to masks
            bg_msk = basicp.mask_padding(bg_msk, int(cfg["frame"]["padding"]))
            items_contours = detector.detect_main_shapes(
                self.img,
                cfg,
                bg_msk,
                bin_treshold=cfg["bin_treshold"],
                sorted=True)

        if obj_type == "grain":
            # add border to take into account frame touching grain
            self.img = cv2.copyMakeBorder(
                     self.img,
                     20, 20, 20, 20,
                     cv2.BORDER_CONSTANT,
                     value=basicp.COLORS['black']
                  )
            bg_msk = cv2.copyMakeBorder(
                     bg_msk,
                     20, 20, 20, 20,
                     cv2.BORDER_CONSTANT,
                     value=basicp.COLORS['black']
                  )
            items_contours = detector.detect_small_shapes(
                self.img, bg_msk, cfg)

        if disp['orig_contours']:
            img_disp = self.img.copy()
            display.draw_contours(img_disp, items_contours,
                                  basicp.COLORS['green'])
            display.show_img(img_disp, 'orig_contours')

        # ---------------
        # For each contour, decide witch item it represents
        new_items = []
        for nb, c in enumerate(items_contours):
            item_inst = detector.detect_item(self.img.copy(), c, obj_type, nb,
                                             4, reference= cfg["reference"]["with"])
            if isinstance(item_inst, Reference):
                if cfg["reference"]["read"] is True:
                    item_inst.read_text()
                item_inst.measure(ref_size[0], ref_size[1])
                self.references.append(item_inst)
            if isinstance(item_inst, (Grain, Ear)):
                self.items.append(item_inst)

        logger.info("Reference founds: {}".format(len(self.references)))
        logger.info("Item founds: {}".format(self._items_count_by_cls()))

        self.img_summary = self.draw_items_contour(self.img.copy())
        # Display detected countours if needed
        if disp['items_contours']:
            display.show_img(self.img_summary, 'items_contours')

    def describe_items(self, obj_type=None):
        """Segment items on different parts."""
        cfg = self.cfg['describe_items']
        disp = cfg['display']

        if obj_type == "ear":
            for i in self._items_by_cls(Ear):
                self.items[i].describe(cfg["ear"])

    def measure_items(self, obj_type=None):
        """ Measure all unmeasured stored items.

        Measures on reference objects give pixel/mm ratio (pixelsPerMetric).
        Measures on ear object give length and width at differents quartiles.
        """
        cfg = self.cfg['measure_items']
        # TODO: Apply a Perspective Transform

        # calculate the pixelsPerMetric on references objects if exists, if many reference object are
        # on the image, we use the mean.
        pix_lst = [
            r.pixelsPerMetric for r in self.references if r.pixelsPerMetric
        ]
        if pix_lst:
            self.pixelsPerMetric = sum(pix_lst) / len(pix_lst)

        # measure every item in pixel
        for i, _ in enumerate(self.items):
            self.items[i].measure(cfg)
            self.items[i].to_metrics(self.pixelsPerMetric, self.correction)

    def get_data(self, with_img=False):
        """Return all data of the items"""
        data = []
        for item in self.items:
            d = {"item": item.__class__.__name__, "item_num": item.number}
            d.update(item.measures_metrics)
            d.update(item.results)
            if with_img:
                d.update({
                    "img_input": self.input_img,
                    "img": item.img,
                    "id_img": self._id_reference,
                    "img_summary": item.img_bounding_summary
                })
                patch_img_dict = flatten(
                    item.img_patch, parent_key='img_patch',
                    sep='/')  # only for exportation as image
                d.update(patch_img_dict)
            data.append(d)
        return data

    def summarize_items(self, obj_type=None):
        # FIXME: see compute_average_ear
        pass

    def get_data_summary(self, with_img=False):
        data = self._items_count_by_cls()
        if with_img:
            data.update({
                "img_input": self.input_img,
                "id_img": self._id_reference,
                "img_summary": self.img_summary
            })
        return data

    def draw_items_contour(self, img):
        if self.references:
            display.draw_contours(img, [
                r.ctr for r in self.references if r.number != -1
            ], basicp.COLORS['blue'])
        display.draw_contours(img, [e.ctr for e in self.items],
                              basicp.COLORS['green'])
        return img
