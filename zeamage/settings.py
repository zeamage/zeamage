import cv2

""" Zeamage configuration file.

Settings are mostly used to adjust zeamage algorithms.
Display values specify wich images have to been shown during processing.
"""


# measures = {
#     # Blur strength to remove noise
#     'blur_factor': 7,
#     # Reference object properties (mm)
#     'ref_width': 760,
#     'ref_length': 127,
#     # Perspective correction between references and ears objects (a*x+b poly)
#     'reference_correction': [1.1148, 0.09],
#     # Dilate-erode hair deletion
#     'open_close': (5, 3),
#     # Dilate-erode noise after holes filling (hairs removing)
#     'shape_open_close': (10, 10),
#     # Erosion before skeletonize
#     'erode_factor': 50,
#     # Minimal relative area for object shape detection
#     'min_detect_area': 0.005,
#     # Binarisation tresholding between 0 - 255
#     'bin_treshold': 20,
#     # Need to be small in order to estimate ear skeleton smoothly
#     'poly_estimation_deg': 2,
#     # Nb of segments to calculate ear length
#     'poly_to_segments': 10,
#     # Quartiles to calculates in addition to median
#     'quartiles': [10, 30, 70, 90],
# }


# gap_detector = {
#     # Clahe contrast increase
#     'clahe_gridsize': 550,
#     # Sobel_factor
#     'sobel': 4,
#     # Max gaps zones
#     'max_gaps': 4,
#     # Gap min area (< than a kernel)
#     'min_area': 0.0004,
#     # Neutral color & sobel blur for pre-tresholding
#     'blur': 29,
#     # Gap detection final tresholding start
#     'g_tresh': 190,
#     # Gaps distriution blur to detect cobs
#     'dist_blur': 23,
#     # Area gaps percentage which ear is considered lacunary
#     'limit_lacunary': 30,
#     # Proportion balance between color and roughness detection methods
#     'balance': [1, 1],
# }


kernel_count = {
    # Clahe specific factor
    'clahe': 35,
    # Clahe unidirectional blur
    'clahe_blur': 81,
    # Times Clahe algorithm is repeated before FFT detection
    'clahe_recursion': 1,
    # Vertical distance between each base segment
    'inter_seg': 5,
    # Minimal segment length (at least 2 * kernel length in pixels)
    'min_seg_len': 170,
    # Maximal length of a kernel (px) to avoid FFT detection errors
    'max_px_len': 100,
    # Individual FFT smoothing before merge
    'fft_blur': 19,
}


row_count = {
    # Clahe specific factor
    'clahe': 42,
    # Horizontal blur after Clahe
    'clahe_blur': 45,
    # Horizontal distance between each base segment
    'inter_seg': 5,
    # Vertical size cover
    'height': 57 / 100,
    # Total ear width cover
    'width': 18 / 100,
    # Minimal segment width in a row
    'min_seg_len': 70,
    # Individual FFT smoothing before merge
    'fft_blur': 7,
    # Row formula correction between
    'row_correction': [1.213, -5.05],
}


display = {
    # Show each image processing step
    'computed_img': False,
    # Supperpose image and mask in order to keep shapes. Erase original image
    'masked_img': False,
    'orig_contours': False,
    # Enhanced visualization of items contours
    'contours': False,
    'contours_color': {
        'ears': 'green',
        'references': 'blue',
    },
    # Drawing measurements on image
    'drawing': False,
    # Show gaps & cob zones on ear
    'gaps': False,
    # Segments representing where FFT analysis is done
    'segs_fft': False,
    # Display kernel width and length frequency graph
    'kernel_fft': False,
    # Line thickness
    'thickness': 2,
    # Max time image is shown
    'timeout': 200000,
}


def set_windows(name):
    """ Set OpenCV images displaying windows to fullscreen with given title.

    Args:
        name (str): Name of the windows
    """
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.setWindowProperty(name, cv2.WND_PROP_FULLSCREEN,
                          cv2.WINDOW_FULLSCREEN)
