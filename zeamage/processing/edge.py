import cv2
import numpy as np

from .display import display
from .contour import smooth


def sobel(channel):
    """ Applies the Sobel Operator to 1 channel image.

    Generates as output an image with the detected edges brighter.
    """
    sobelX = cv2.Sobel(channel, cv2.CV_16S, 1, 0)
    sobelY = cv2.Sobel(channel, cv2.CV_16S, 0, 1)
    sobel = np.hypot(sobelX, sobelY)
    # Limit values to RGB standard (0-255)
    sobel[sobel > 255] = 255
    return sobel


@display
def sobel_colored(img, noise_rm_factor=1):
    """ Applies the Sobel Operator on colored image.

    Generates an image with where detected edges are brighter.

    Args:
        img (np.ndarray):      image to process
        noise_rm_factor (int): power of noise reduction
    Returns:
        Image with the detected edges bright on a darker background
    """
    omg = np.max(np.array([sobel(img[:, :, 0]),
                           sobel(img[:, :, 1]),
                           sobel(img[:, :, 2])]),
                 axis=0)
    mean = np.mean(omg)
    # Zero any value that is less than mean. This reduces a lot of noise.
    omg[omg <= mean * noise_rm_factor] = 0
    omg = np.asarray(omg, np.uint8)
    return omg


@display
def clahe(img, gridsize=100):
    """ Enhance local contrast of image using CLAHE algorithm.

    Contrast Limited AHE implementation using openCV.
    https://en.wikipedia.org/wiki/Adaptive_histogram_equalization.

    Args:
        img (np.ndarray): Image to process
        gridsize (int):   Neighbourhood region. contrast at smaller scales is
                          enhanced while contrast at larger scales is reduced
    Returns:
        Computed equalized image
    """
    lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    lab_planes = cv2.split(lab)
    clahe = cv2.createCLAHE(clipLimit=4.0,
                            tileGridSize=(gridsize, gridsize))
    lab_planes[0] = clahe.apply(lab_planes[0])
    lab = cv2.merge(lab_planes)
    return cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)



def kmeans(img, K=10):

    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    Z = img.reshape((-1, 3))
    Z = np.float32(Z)
    ret, labels, centers = cv2.kmeans(Z, K, None, criteria, 10,
                                    cv2.KMEANS_RANDOM_CENTERS)

    # Now convert back into uint8, and make original image
    centers = np.uint8(centers)
    res = centers[labels.flatten()]
    return res.reshape((img.shape))


def fft(vals, num, blur_f=0):
    """Computes Fast Fourier Transform of an array of points.

    Args:
        vals (np.array): Array of values
        num (int):       Number of values in output array
        blur_f (int):    Blur factor applied to signal after FFT
    Returns:
        Frequency FFT transform of given signal
    """
    freq_vals = abs(np.fft.fft(vals, n=num))[1:]
    return smooth(np.array(freq_vals), blur_f)
