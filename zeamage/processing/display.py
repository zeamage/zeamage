import cv2
import numpy as np
from matplotlib import pyplot as plt

from zeamage.settings import display as disp
from zeamage.settings import set_windows
from zeamage.processing import basicp


def display(func):
    """ Decorator function that displays results from image algorithms.
    """
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        if disp['computed_img'] and type(response) is np.ndarray:
            show_img(response, func.__name__)
        return response
    return wrapper


def show_img(img, name='default'):
    """ Simple function that display an image.

    Args:
        img (np.ndarray): Image to display
        name (str):       Name of the image window
    """
    cv2.imshow(name, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


@display
def draw_lines(img, segments, color, thickness=None):
    """ Draws and returns a list of segments on an image.

    Args:
        img (np.ndarray): Image to draw the lines on
        segments (list):  List of all the couples of points
        color (set):      Segments color
        thickness:        Segments thickness
    Returns:
        Image with the drawn segments
    """
    for s in segments:
        if not thickness:
            thickness = disp['thickness']
        img = cv2.line(img, s[0], s[1], color, thickness)
    return img

@display
def draw_label(img, labels, color, alpha):
    """ Draws and returns a list of segments on an image.

    Args:
        img (np.ndarray): Image to draw the lines on
        segments (list):  List of all the couples of points
        color (set):      Segments color
        thickness:        Segments thickness
    Returns:
        Image with the drawn segments
    """
    overlay_img = np.zeros(img.shape, np.uint8)
    overlay_img[:,:] = color

    overlay_msk = cv2.bitwise_and(overlay_img, overlay_img, mask=labels)
    cv2.addWeighted(overlay_msk, alpha, img, 1- alpha, 0, img)
    return img


@display
def draw_grid(img, step_x, step_y, color, thickness=None):
    """ Draws a grid on given image then returns it.

    Args:
        img (np.ndarray): Image to draw the grid on
        step_x (int):     Horizontal interval in pixels between each column
        step_y (int):     Vertical interval in pixels between each line
        color (set):      Grid lines color
        thickness:        Grid lines thickness
    Returns:
        Image with the drawn grid
    """
    length, height = img.shape[:2]
    if not thickness:
        thickness = disp['thickness']
    for x in range(step_x, height, step_x):
        cv2.line(img, (x, 1), (x, length), color, thickness)
    for y in range(step_y, length, step_y):
        cv2.line(img, (1, y), (height, y), color, thickness)
    return img


def draw_contours(img, contours, color=None, thickness=1):
    """ Display a list of countours above an image.

    Args:
        img (np.ndarray): Image to process.
        contours (list):  List of contours.
        color (set):      Color used to display contours.
    """
    if not color:
        color = basicp.COLORS['green']
    if not thickness:
        thickness = disp['thickness']
    cv2.drawContours(img, contours, -1, color,
                              thickness,
                              cv2.LINE_AA,
                              maxLevel=2)

@display
def add_mask(img, contours, color):
    """ Keep image parts that are inside given countours.

    Args:
        img (np.ndarray): Image to process.
        contours (list):  List of contours.
        color (set):      Color applied to removed parts.
    Returns:
        Resulting image
    """
    # Determine mask from contour
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    msk = basicp.mask(gray_img, contours, color)
    # Change each pixel outside the mask to desired color
    img[np.logical_not(msk)] = color
    return img


def plot(data, title='Default Title', text=None, vert=None, labels=['',''], legend=None):
    """ Plot and display datas on a graph.

    Args:
        data (np.ndarray):   Either 1d or 2d array (x, y)
        title (str):         Windows title
        text (array[str]):   Add text on the top right
        vert (None|int):     If type is int draw a vertical line on given value
        labels ([str, str]): X-axis and y-axis labels
        legend (str):        Name to give to drawn points
    """
    if len(data) == 1:
        plt.plot(data[0])
    else:
        plt.plot(data[0], data[1])
    if vert:
        plt.axvline(x=vert, color='orange')
    plt.title(title)
    plt.xlabel(labels[0])
    plt.ylabel(labels[1])
    if legend:
        plt.legend([legend])
    if text:
        plt.text(max(data[0]), max(data[1]),
            text, horizontalalignment='right',
            verticalalignment='top')
    plt.draw()
    plt.waitforbuttonpress(0) # this will wait for indefinite time
    plt.close()


def write_shape_dimensions(img, img_mask, len_text, width_text, color):
    """ Draw dimensions of a shape defined by mask over image.

    Args:
        img (np.ndarray):      Image to draw dimensions on
        img_mask (np.ndarray): Mask that define corresponding dimensions
        len_text (str):        Text written on horizontal measure
        width_text (str):      Text written on vertical measure
        color (set):           Color of each drawn element
    Returns:
        Image with all measures drawn
    """
    y1, y2, x1, x2 = basicp.mask_shape(img_mask)
    width, length = img.shape[:2]
    # Drawn the horizontal lines
    cv2.line(img, (int((x1+x2)/2), y1), (5, y1), color, 1)
    cv2.line(img, (int((x1+x2)/2), y2), (5, y2), color, 1)
    # Put arrows and width text corresponding to vertical measure
    cv2.arrowedLine(img, (20, y1), (20, y2), color, 1,
                    tipLength=20 / (y2 - y1))
    cv2.arrowedLine(img, (20, y2), (20, y1), color, 1,
                    tipLength=20 / (y2 - y1))
    cv2.putText(img, '{:.1f} {}'.format(width_text, 'mm'),
                (30, int((y2+y1)/2)),
                cv2.FONT_HERSHEY_SIMPLEX,
                1.2, color, 1, cv2.LINE_AA)
    # Drawn the vertical lines
    cv2.line(img, (x1, width-5), (x1, int((y1+y2)/2)), color, 1)
    cv2.line(img, (x2, width-5), (x2, int((y1+y2)/2)), color, 1)
    # Put arrows and len text corresponding to horizontal measure
    cv2.arrowedLine(img, (x1, width-20), (x2, width-20), color, 1,
                    tipLength=20/(x2-x1))
    cv2.arrowedLine(img, (x2, width-20), (x1, width-20), color, 1,
                    tipLength=20/(x2-x1))
    cv2.putText(img, '{:.1f} {}'.format(len_text, 'mm'),
                (int((x1+x2)/2), width-30),
                cv2.FONT_HERSHEY_SIMPLEX,
                1.2, color, 1, cv2.LINE_AA)
    return img


def mask_segments(img, msk):
    """ Draw segments on image corresponding to the square fitting mask shape.

    Args:
        img (np.ndarray): Image to drawn segments on
        msk (np.ndarray): Black and white mask defining shape to frame
    Returns:
        Drawn image
    """
    y1, y2, x1, x2 = basicp.mask_shape(msk)
    cv2.line(img, (x1, 1), (x1, img.shape[0]), basicp.COLORS['blue'], 2)
    cv2.line(img, (x2, 1), (x2, img.shape[0]), basicp.COLORS['blue'], 2)
    cv2.line(img, (1, y1), (img.shape[1], y1), basicp.COLORS['blue'], 2)
    cv2.line(img, (1, y2), (img.shape[1], y2), basicp.COLORS['blue'], 2)
    return img
