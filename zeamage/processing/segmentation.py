import logging
import sys
from pathlib import Path
import math
import numpy as np

import cv2
from skimage import segmentation
from skimage.future import graph

logger = logging.getLogger(__name__)
#
#
# def label_colorfulness(pixels):
#     # split the image into its respective RGB components, then mask
#     # each of the individual RGB channels so we can compute
#     # statistics only for the masked region
#     (B, G, R) = pixels.T
#
#     # compute rg = R - G
#     rg = np.absolute(R - G)
#
#     # compute yb = 0.5 * (R + G) - B
#     yb = np.absolute(0.5 * (R + G) - B)
#
#     # compute the mean and standard deviation of both `rg` and `yb`,
#     # then combine them
#     stdRoot = np.sqrt((rg.std() ** 2) + (yb.std() ** 2))
#     meanRoot = np.sqrt((rg.mean() ** 2) + (yb.mean() ** 2))
#
#     # derive the "colorfulness" metric and return it
#     return stdRoot + (0.3 * meanRoot)
#
#
# def rag_color(image, labels, connectivity=2, mode='distance', sigma=255.0):
#     """Compute the Region Adjacency Graph using mean colors.
#     Given an image and its initial segmentation, this method constructs the
#     corresponding Region Adjacency Graph (RAG). Each node in the RAG
#     represents a set of pixels within `image` with the same label in `labels`.
#     The weight between two adjacent regions represents how similar or
#     dissimilar two regions are depending on the `mode` parameter.
#     Parameters
#     ----------
#     image : ndarray, shape(M, N, [..., P,] 3)
#         Input image.
#     labels : ndarray, shape(M, N, [..., P])
#         The labelled image. This should have one dimension less than
#         `image`. If `image` has dimensions `(M, N, 3)` `labels` should have
#         dimensions `(M, N)`.
#     connectivity : int, optional
#         Pixels with a squared distance less than `connectivity` from each other
#         are considered adjacent. It can range from 1 to `labels.ndim`. Its
#         behavior is the same as `connectivity` parameter in
#         ``scipy.ndimage.generate_binary_structure``.
#     mode : {'distance', 'similarity'}, optional
#         The strategy to assign edge weights.
#             'distance' : The weight between two adjacent regions is the
#             :math:`|c_1 - c_2|`, where :math:`c_1` and :math:`c_2` are the mean
#             colors of the two regions. It represents the Euclidean distance in
#             their average color.
#             'similarity' : The weight between two adjacent is
#             :math:`e^{-d^2/sigma}` where :math:`d=|c_1 - c_2|`, where
#             :math:`c_1` and :math:`c_2` are the mean colors of the two regions.
#             It represents how similar two regions are.
#     sigma : float, optional
#         Used for computation when `mode` is "similarity". It governs how
#         close to each other two colors should be, for their corresponding edge
#         weight to be significant. A very large value of `sigma` could make
#         any two colors behave as though they were similar.
#     Returns
#     -------
#     out : RAG
#         The region adjacency graph.
#     Examples
#     --------
#     >>> from skimage import data, segmentation
#     >>> from skimage.future import graph
#     >>> img = data.astronaut()
#     >>> labels = segmentation.slic(img)
#     >>> rag = graph.rag_mean_color(img, labels)
#     References
#     ----------
#     .. [1] Alain Tremeau and Philippe Colantoni
#            "Regions Adjacency Graph Applied To Color Image Segmentation"
#            :DOI:`10.1109/83.841950`
#     """
#     G = graph.RAG(labels, connectivity=connectivity)
#
#     # add custom node attribute
#     for n in G:
#
#         G.node[n].update({'labels': [n],
#                               'pixel count': 0,
#                               'pixels': [],
#                               'total color': np.array([0, 0, 0],
#                                                       dtype=np.double)})
#
#     for index in np.ndindex(labels.shape):
#         current = labels[index]
#         G.node[current]['pixel count'] += 1
#         G.node[current]['total color'] += image[index]
#         G.node[current]['pixels'].append(image[index])
#
#
#     for n in G:
#         G.node[n]['mean color'] = (G.node[n]['total color'] /
#                                        G.node[n]['pixel count'])
#
#         G.node[n]['pixels'] = np.array(G.node[n]['pixels'])
#         G.node[n]['colorfullness'] = label_colorfulness(G.node[n]['pixels'] )
#
#
#     for x, y, d in G.edges(data=True):
#         diff = G.node[x]['mean color'] - G.node[y]['mean color']
#         diff = np.linalg.norm(diff)
#         if mode == 'similarity':
#             d['weight'] = math.e ** (-(diff ** 2) / sigma)
#         elif mode == 'distance':
#             d['weight'] = diff
#         else:
#             raise ValueError("The mode '%s' is not recognised" % mode)
#
#     return G



def _weight_mean_color(G, src, dst, n):
    """Callback to handle merging nodes by recomputing mean color.

    The method expects that the mean color of `dst` is already computed.

    Parameters
    ----------
    graph : RAG
        The graph under consideration.
    src, dst : int
        The vertices in `graph` to be merged.
    n : int
        A neighbor of `src` or `dst` or both.

    Returns
    -------
    data : dict
        A dictionary with the `"weight"` attribute set as the absolute
        difference of the mean color between node `dst` and `n`.
    """

    diff = G.node[dst]['mean color'] - G.node[n]['mean color']
    diff = np.linalg.norm(diff)
    return {'weight': diff}

#
# def _weight_colorfullness(G, src, dst, n):
#     """Callback to handle merging nodes by recomputing colorfullness.
#
#     The method expects that the mean color of `dst` is already computed.
#
#     Parameters
#     ----------
#     graph : RAG
#         The graph under consideration.
#     src, dst : int
#         The vertices in `graph` to be merged.
#     n : int
#         A neighbor of `src` or `dst` or both.
#
#     Returns
#     -------
#     data : dict
#         A dictionary with the `"weight"` attribute set as the absolute
#         difference of the mean color between node `dst` and `n`.
#     """
#
#     diff = G.node[dst]['colorfullness'] - G.node[n]['colorfullness']
#     diff = np.linalg.norm(diff)
#     return {'weight': diff}


def _merge_mean_color(graph, src, dst):
    """Callback called before merging two nodes of a mean color distance graph.

    This method computes the mean color of `dst`.

    Parameters
    ----------
    graph : RAG
        The graph under consideration.
    src, dst : int
        The vertices in `graph` to be merged.
    """
    graph.node[dst]['total color'] += graph.node[src]['total color']
    graph.node[dst]['pixel count'] += graph.node[src]['pixel count']
    graph.node[dst]['mean color'] = (
        graph.node[dst]['total color'] / graph.node[dst]['pixel count'])
#
#
# def _merge_colorfullness(G, src, dst):
#     """Callback called before merging two nodes of a mean color distance graph.
#
#     This method computes the mean color of `dst`.
#
#     Parameters
#     ----------
#     graph : RAG
#         The graph under consideration.
#     src, dst : int
#         The vertices in `graph` to be merged.
#     """
#     G.node[dst]['pixels'] = np.concatenate((G.node[dst]['pixels'], G.node[src]['pixels']))
#     G.node[dst]['colorfullness'] = label_colorfulness(G.node[dst]['pixels'] )



def segment_mean_color(img,
                       compactness=50,
                       num_segments=200,
                       agg_threshold=20,
                       merge=True):
    sgt = segmentation.slic(
        img,
        compactness=compactness,
        sigma=0.5,
        n_segments=num_segments,
        slic_zero=False)

    # some segment cleaning (OPTIONAL)
    # sgt = segmentation.clear_border(sgt)

    if merge == True:
        rag = graph.rag_mean_color(img, sgt, mode="distance")
        return graph.merge_hierarchical(
            sgt,
            rag,
            thresh=agg_threshold,
            rag_copy=False,
            in_place_merge=True,
            merge_func=_merge_mean_color,
            weight_func=_weight_mean_color)
    return sgt
