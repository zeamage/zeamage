import cv2
import numpy as np
import logging

from zeamage.processing import display

logger = logging.getLogger(__name__)


def skeletonize(img):
    """ Keep skeletons of shapes presents in the image.

    Hat tip to http://felix.abecassis.me/2011/09/opencv-morphological-skeleton.

    Args:
        img (np.ndarray): Image to proceed
    Returns:
        Mat object
    """
    # Local img copy needed because of transformations
    img = img.copy()
    skel = img.copy()
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
    while True:
        eroded = cv2.morphologyEx(img, cv2.MORPH_ERODE, kernel)
        temp = cv2.morphologyEx(eroded, cv2.MORPH_DILATE, kernel)
        temp = cv2.subtract(img, temp)
        img[:, :] = eroded[:, :]
        if not cv2.countNonZero(img):
            break
        skel = cv2.bitwise_or(skel, temp)
    return skel


def skeleton_line_recursive(img, erode_iter):
    simg = cv2.erode(img, (3, 3), iterations=erode_iter)
    # logger.debug(erode_iter)
    if cv2.countNonZero(simg) < 50 and not erode_iter <= 1:
        erode_iter -= 1
        return skeleton_line_recursive(img, erode_iter)
    return simg


@display.display
def skeleton_line(msk, erode_iter=4):
    """ Keep skeletons of shapes presents in the image.

    Args:
        msk (np.ndarray): Black and white image defining shape to skeletonize
        erode_iter (int): Number of iteration in skeletonize process
    Returns:
        Black image whith few points representing found skeleton line
    """
    simg = skeletonize(msk)
    if cv2.countNonZero(simg) < 50:
        return simg
    return skeleton_line_recursive(simg, erode_iter)


@display.display
def erode(msk, erode_factor, iterations=1):
    """ Erode a mask with erode_factor strength.
    """
    while True:
        kernel = np.ones((erode_factor, erode_factor), np.uint8)
        erode_msk = cv2.erode(msk, kernel, iterations)
        if cv2.countNonZero(erode_msk) < 5:
            erode_factor -= 1
        else:
            break
    return erode_msk
