import cv2
import numpy as np

from .display import display
import logging

logger = logging.getLogger(__name__)


COLORS = {
    'black': (0, 0, 0),
    'white': (255, 255, 255),
    'blue': (255, 0, 0),
    'green': (0, 255, 0),
    'red': (0, 0, 255),
}


def contour2mask(img, contours):
    """ Transform image to a simple black and white mask depending on
    given contours.
    Args:
        img (np.ndarray):      Original image needed to know dimensions
        contours (np.ndarray): Array of polygons where each polygon is
                               represented as an array of points
    Returns:
        Black and white np.ndarray image representing a mask of the contour
    """
    mask = np.zeros(img.shape[:2], np.uint8)
    cv2.fillPoly(mask, contours, 255)
    return mask


def mask_padding(msk, pad=0):
    pad_msk = np.zeros(msk.shape[:2], np.uint8)
    h, w = msk.shape[:2]
    pad_msk[pad:h - pad, pad:w - pad] = 255

    msk[pad_msk==0] = 0
    return msk


@display
def binary(img, treshold=None):
    """ Convert orignal image to binary one.
    Args:
        img (np.ndarray):    Image to process
        treshold (None|int): Otsu method if None, classic treshold otherwise
    Returns:
        Black and white tresholded image
    """
    if treshold:
        # Tresholding between [0-255] values
        retval, bin_img = cv2.threshold(img, treshold, 255, cv2.THRESH_BINARY)
    else:
        retval, bin_img = cv2.threshold(img, 0, 255,
                                        cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return bin_img


def gray(img):
    """ Convert image to grayscale.
    """
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


def color(img):
    """ Transform a 1 channel image to RGB multichannel one.
    """
    return cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)


def crop_rectangle(img, ctr, padding=30, type="minAreaRect"):
    pw = padding  # padding
    ph = padding  # padding

    if type == "minAreaRect":
        rect = cv2.minAreaRect(ctr)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        W = rect[1][0]
        H = rect[1][1]

        Xs = [i[0] for i in box]
        Ys = [i[1] for i in box]
        x1 = min(Xs)
        x2 = max(Xs)
        y1 = min(Ys)
        y2 = max(Ys)

        angle = rect[2]
        if angle < -45:
            angle += 90

        # Center of rectangle in source image
        center = ((x1+x2)/2,(y1+y2)/2)
        # Size of the upright rectangle bounding the rotated rectangle
        size = (x2-x1, y2-y1)
        M = cv2.getRotationMatrix2D((size[0]/2, size[1]/2), angle, 1.0)
        # Cropped upright rectangle
        cropped = cv2.getRectSubPix(img, size, center)
        cropped = cv2.warpAffine(cropped, M, size)
        croppedW = H if H > W else W
        croppedH = H if H < W else W
        # Final cropped & rotated rectangle
        img =  cv2.getRectSubPix(cropped, (int(croppedW),int(croppedH)), (size[0]/2, size[1]/2))
        h, w = img.shape[:2]
        return img[ph:h - ph, pw:w - pw]

    else:
        x, y, w, h = cv2.boundingRect(ctr)
        return img[y + ph:y + h - ph, x + pw:x + w - pw]



@display
def rotate(image, angle):
    """ Adrian Rosebrock function (imutils).

    website: http://www.pyimagesearch.com
    """
    # grab the dimensions of the image and then determine the center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)
    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY
    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))


def mask_diff(msk1, msk2):
    """ Compute two masks area ratio.
    """
    return np.sum(msk1) / np.sum(msk2)


def mask_shape(msk):
    """ Gives shape of a mask image.

    Args:
        msk (np.ndarray): black and white mask image
    Return:
        y1, y2, x1, x2: extremum coordinates
    """
    v = np.nonzero(msk)
    return v[0].min(), v[0].max(), v[1].min(), v[1].max()
