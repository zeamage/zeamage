import numpy as np
import math
import cv2

from .display import display, draw_lines
from . import shape

import logging

logger =logging.getLogger(__name__)


def estimate_skel_angle(coord, f1=0.002644, f2=0.75):
    """ Estimate angle of a points cloud in an image.

    Approximation is given using x and y variance of the cloud of points.
    Parameters f1 and f2 have been determined experimentaly.

    Args:
        img (np.ndarray): skeleton of image, 1d image of 0|255 values.
    Return:
        angle: estimation of angle between -90 and 90 deg.
    """
    try:
        diff = np.var(coord[1]) / np.var(coord[0])
        # Quite unrefined approximation
        angle = 1 / (np.power((diff * f1), f2) + 1/90)
        # Invert angle if necessary, also unrefined
        angle = angle * np.sign(coord[-1][-1] - coord[-1][1])
        angle = 90 + angle
    except Exception as e:
        logger.info("Could not estimate skel angle")
        angle =  0
    return angle


def skel_to_polynom(x, y, poly_deg):
    """ Polynomial regression to fit a cloud of points.

    Args:
        x (np.array): X coordinates of every points
        y (np.array): Y coordinates of every points
    Returns:
        Function with type np.poly1d
    """
    poly_val = np.polyfit(x, y, poly_deg)
    return np.poly1d(poly_val)


def horizontal_intersec(poly, mask):
    """ Find first and last x-axis points of polynom function matching a mask.

    Args:
        poly (polynom):    Polynom that describe horizontal curve
        mask (np.ndarray): Mask used to determine extremum matching points
    Returns:
        x-axis values of minimum and maximum horizontal matching points
    """
    # Poly to coordinates
    y_values = [int(poly(x)) for x in range(len(mask[0]))]
    # Keep values that are fitting image size
    y_values = [1 if y < 1 else y for y in y_values]
    y_values = [len(mask) - 1 if y >= len(mask) else y for y in y_values]
    # Look mask value along the polynom values
    projection = [mask[y_values[x], x] for x in range(len(mask[0]))]
    # Keep first and last 0 values, so mathing the mask
    coords = np.where(projection)[0]
    return coords[0], coords[-1]


def lin_vertical_intersec(point, coef, origin_y, mask):
    """ Find first and last y-axis points of linear polynom matching a mask.

    Args:
        point ((int, int)): Polynom that describe horizontal curve
        coef (None|int):    polynom coefficient, None if infinite (vertical)
        origin_y (int):     Y-axis polynom origin
        mask (np.ndarray):  Mask used to determine extremum matching points
    Returns:
        y-axis values of minimum and maximum vertical matching points
    """

    if not coef:
        # Line is totaly vertical
        x_cst = int(point[0])
        projection = [mask[y, x_cst] for y in range(len(mask))]
    else:
        # We use invert function f(y) = x
        f = lambda y: (y - origin_y)/coef
        x_values = [int(f(y)) for y in range(len(mask))]
        x_values = [1 if x < 1 else x for x in x_values]
        x_values = [len(mask[0]) - 1 if x >= len(mask[0])
                    else x for x in x_values]
        projection = [mask[y, x_values[y]] for y in range(len(mask))]
        projection = [p if p > 0 else 0 for p in projection]
    coords = np.where(projection)[0]
    return coords[0], coords[-1]


def measure_segment(s):
    """ Gives length of a segment.

    Args:
        s (set): Couple of points representing segment
    Returns:
        Segement length
    """
    return math.hypot(s[1][0] - s[0][0], s[1][1] - s[0][1])


def tangente_segs(lon, segments_len, segments):
    """ Determine tangent to a skeleton represented by succesive segments
    at a given distance.

    Args:
        lon (int):           Distance from start where to determine tangent
        segments_len (list): List containing length of all successive segments
        segments (list):     List of each segment as couple of points
    Returns:
        The point where tagent fit skeleton and parameters of a 1d polynom
        representing normal line
    """
    segn = 0
    # Determine on which segment it will be
    while sum(segments_len[0:segn + 1]) < lon:
        segn += 1
    # Calculate distance on this segment
    dist_seg = lon - sum(segments_len[0:segn])
    seg_x = (segments[segn][0][0], segments[segn][1][0])
    seg_y = (segments[segn][0][1], segments[segn][1][1])
    diff_x = abs(seg_x[1] - seg_x[0])
    diff_y = abs(seg_y[1] - seg_y[0])
    coord_x = seg_x[0] + diff_x * (dist_seg / segments_len[segn])
    if seg_y[0] >= seg_y[1]:
        coord_y = seg_y[0] - diff_y * (dist_seg / segments_len[segn])
    else:
        coord_y = seg_y[0] + diff_y * (dist_seg / segments_len[segn])
    coords = [int(coord_x), int(coord_y)]
    coef = origin_y = None
    if seg_y[1] != seg_y[0]:
        coef = - (seg_x[1] - seg_x[0]) / (seg_y[1] - seg_y[0])
        origin_y = coords[1] - (coords[0] * coef)
    return coords, coef, origin_y


def determine_dimensions(poly, mask, seg_nb, quartiles):
    """ Determine full dimensions of an item defined by his skeleton and a
    mask that represents his shape.

    Args:
        poly (np.poly1d):  Polynom describing the skeleton of the shape
        mask (np.ndarray): Mask defining item shape around the skeleton
        seg_nb (int):      Number of used segments in the measured skeleton
        quartiles (list):  List of quartiles to add to center measure
    Returns:
        A dict of segments  and measure representing computed quartiles, mediabn line
    """
    segs = {}

    extremums = horizontal_intersec(poly, mask)
    total_dist = extremums[1] - extremums[0]
    # Use seg_nb segments to represent ear skeleton
    seg_len = int(total_dist / seg_nb)
    # Determine segments corresponding points
    points = [(x, int(poly(x))) for x in range(extremums[0],
                                               extremums[1],
                                               seg_len)]
    # Add last segment point if missing in previous range
    if points[-1][1] < extremums[1]:
        points.append((extremums[1], int(poly(extremums[1]))))

    # Create segments
    segments = []
    segments += [(points[p], points[p+1]) for p in range(len(points) - 1)]
    base_segments = segments.copy()
    segments_len = [measure_segment(s) for s in segments]

    segs["L"] = {
        "segment":[(points[p], points[p+1]) for p in range(len(points) - 1)],
        "measure": sum(segments_len)
    }

    quartiles_segs = [[], []]
    for q in quartiles:
        qlen = int(segs["L"]['measure'] * (q / 100))
        # Determine central point and tangent orientation
        center_point, coef, origin_y = tangente_segs(qlen,
                                                     segments_len,
                                                     base_segments)
        # Store y coordinates of extremum vertical points
        intersec_y = list(lin_vertical_intersec(center_point, coef,
                                                origin_y, mask))
        # Find quartile extremum points
        quart_points = [[], []]
        if not coef:
            quart_points[0] = (int(center_point[0]), intersec_y[0])
            quart_points[1] = (int(center_point[0]), intersec_y[1])
        else:
            inv = lambda y: (y - origin_y) / coef
            quart_points[0] = (int(inv(intersec_y[0])), intersec_y[0])
            quart_points[1] = (int(inv(intersec_y[1])), intersec_y[1])
        # Translate points to segments and store them
        seg = (quart_points[0], quart_points[1])

        segs["l" + str(q)] = {
            "measure":measure_segment(seg),
            "segment":[seg]
        }

    return segs


def quartiles_points(poly, quartiles, xmin, xmax):
    """ Determine a list of points representing ear countour at each quartile.

    Args:
        poly (np.poly1d): Skeleton polynomial description
        quartiles (dict): Dictionary of {percentage: width} quartiles
        xmin (int):       X-axis start of the shape
        xmax (int):       X-axis end of the shape
    Returns:
        Top contour points and bottom countour points in inverse order
    """
    # Determinate all quartile points
    nquart = len(quartiles)
    quart_x = [int(xmin + (loc / 100) * (xmax - xmin)) for loc in quartiles]
    # Calculating tangent points
    top_pts = []
    bottom_pts = []
    for q, l in zip(quart_x, quartiles.values()):
        # FIXME Use gradient to get precise locations
        top_pts.append((q, int(poly(q) + l/2)))
        bottom_pts.append((q, int(poly(q) - l/2)))
    return top_pts, bottom_pts[::-1]


def masked_segments(segments_pts, shape, msk, axis=False):
    """ Returns cropped segments that match mask.

    Segements should be given as a list of successive points.

    Args:
        segments_pts (list): Lists of y-axis points representing a segment
        shape (int, int):    X-axis extremum coordinates of the segments
        msk (np.ndarray):    Mask image
        axis (bool):         Invert axis (Y-axis masked segments)
    Returns:
        Fragmented segments list as lists of points
    """
    frag_segments = []
    index = -1
    new_seg = False
    for s in segments_pts:
        frag_segments.append([shape[0]])
        index += 1
        if axis:
            coords = zip(range(shape[0], shape[1]), s)
        else:
            coords = zip(s, range(shape[0], shape[1]))
        for p, x in coords:
            if not msk[int(p), int(x)]:
                new_seg = True
                continue
            if new_seg:
                frag_segments.append([int(p) if axis else int(x)])
                index += 1
                new_seg = False
            frag_segments[index] += [int(x) if axis else int(p)]
    return frag_segments


def populate_segments(base, inter_seg, min_len, msk, shape, axis=False, cut=0):
    """ Generate a list of parallel segments as lists of points in a
    specified zone of a mask.

    Args:
        base (list):      Base segment as a list of points
        inter_seg (int):  Space in pixels between each generated segment
        min_len (int):    Do not keep segments which size < min_len
        msk (np.ndarray): Mask image that segments should match
        shape (list):     y1, y2, x1, x1 representing
        axis (bool):       Y-axis populate if set to True
        cut (int):        Percentage to cut from segments extremums
    Returns:
        List of segments. For each segments first element represent the
        first x-axis value and segment[1:] represent y-axis values for each
        successively incremented x-axis values.
    """
    y1, y2, x1, x2 = shape
    # index of inter segments, ex : [-2, 0, 2, 4]
    sl = np.array([(j + 1)/2 if (j % 2) else -j / 2
                  for j in range(1, int((y2 - y1) / inter_seg))], int)
    cutted = int((len(base) * cut) / 2)
    segments_pts = [(base - s * inter_seg)[cutted:-(cutted + 1)] for s in sl]
    segments_pts.append(base[cutted:-(cutted + 1)])
    # Keep segments that match mask
    frag_segments = masked_segments(segments_pts,
                                    (x1 + cutted, x2),
                                    msk,
                                    axis=axis)
    # Keep interesting segments (> min len)
    segs = [f for f in frag_segments if len(f) > min_len]
    return segs
