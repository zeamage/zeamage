import cv2
import numpy as np
from .display import display


@display
def blur(img, force, uni=0):
    """ Blur image

    Args:
        img (np.ndarray): Image to blur
        force (int):      Neighborhood size of blur
        uni (int):        Both size if 0, y-axis if odd else x-axis
    Returns:
        Corresponding blurred image
    """
    if not uni:
        ksize = (force, force)
    else:
        ksize = (force, 1) if uni % 2 else (1, force)
    return cv2.GaussianBlur(img, ksize, 0)


@display
def dilate_erode(img, f_open, f_close):
    """ Apply successively opening morphological transformation (erosion
    followed by dilation) then closing (dilation followed by erosion).

    Args:
        img (np.ndarray):     Image to transform
        f_open ((int, int)):  Erode and dilate strength opening
        f_close ((int, int)): Dilate and erode strength closing
    Returns:
        Transformed image
    """
    open_kernel = np.ones((f_open, f_open), np.uint8)
    # Opening in order to remove external noise
    opened = cv2.morphologyEx(img, cv2.MORPH_OPEN, open_kernel)
    # Closing in order to remove small holes inside the foreground objects
    close_kernel = np.ones((f_close, f_close), np.uint8)
    return cv2.morphologyEx(opened, cv2.MORPH_CLOSE, close_kernel)
