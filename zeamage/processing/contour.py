import numpy as np

import cv2
from scipy.signal import savgol_filter

from .display import display
from .basicp import contour2mask

def smooth(contours, smth=30):
    """ Smooth an array of points.

    Args:
        contours (np.ndarray): Contour to be smoothed
        smth (int):            x and y-axis smooth force
    Returns:
        Smoothed contour
    """
    ksize = (smth, smth)
    return cv2.blur(contours, ksize)[:, 0]


def keep(b_img, max_zones, min_area):
    """ Keep zones with biggest areas on a black and white image.

    Args:
        b_img (np.ndarray): Black image whith white zones
        max_zones (int):    Maximun of zones in returned image
        min_area (float):   Minimum area necessary to keep a zone
    Returns:
        Input image were only biggest zones are kept"""
    ctr = find_outermost(b_img, min_area)
    mask = contour2mask(b_img, ctr[-max_zones:])
    return mask & b_img


def find_outermost(img, min_area):
    """ Find outermost contours in image from opencv contour tree hierarchy.

    Args:
        img (np.ndarray): Image to search for contours
        min_area (float): Part of the total area representing minimal item size
    Returns:
        Significant outermost contours
    """
    # Keep only outermost contours
    i, contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL,
                                              cv2.CHAIN_APPROX_SIMPLE)
    significant = []
    # From among them, find contours with larger surface area
    for ctr in contours:
        area = check_area(ctr, img, min_area)
        if area is not None and check_inner(ctr, img):
            # Store contours
            significant.append([ctr, area])

    significant.sort(key=lambda x: x[1])
    significant = [x[0] for x in significant]
    return significant


def smooth_contour(ctr, img, smth=0.05):
    # Use Savitzky-Golay filter to smoothen contour.
    window_size = int(round(min(img.shape[0], img.shape[1]) * smth)) # Consider each window to be 5% of imag dim
    x = savgol_filter(ctr[:, 0, 0], window_size * 2 + 1, 3)
    y = savgol_filter(ctr[:, 0, 1], window_size * 2 + 1, 3)

    approx = np.empty((x.size, 1, 2))
    approx[:, 0, 0] = x
    approx[:, 0, 1] = y
    approx = approx.astype(int)
    return approx


def check_area(ctr, img, min_area):
    # Keep contours fitting covering min_area of total area
    tooSmall = img.size * min_area
    area = cv2.contourArea(ctr)
    if area > tooSmall:
        return area
    return None


def check_inner(ctr, img):
    # If contour is outside picture, do not conclude, i.e. remove contour that touch the image
    # border, object are potentially not complete
    width = img.shape[1]
    height = img.shape[0]
    for pt in ctr:
        if pt[0][0] in (0, width - 1) or pt[0][1] in (0, height - 1):
            return False
    return True


@display
def fill_holes(bin_img):
    """ Fill holes in every subsequent shapes of a black and white image.
    """
    ext_filled = bin_img.copy()
    cv2.floodFill(ext_filled, None, (0, 0), 255)
    return np.invert(ext_filled) | bin_img


def sort_contours(cnts, method="left-to-right"):
    """Sort contour, usefull for build id from read text on reference object"""
    # initialize the reverse flag and sort index
    reverse = False
    i = 0

    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True

    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1

    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))

    # return the list of sorted contours and bounding boxes
    return cnts, boundingBoxes
