from pathlib import Path
import cv2
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import accuracy_score
import sys

# Importing the Keras libraries and packages
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.preprocessing.image import ImageDataGenerator



import numpy as np

import logging

logger = logging.getLogger(__name__)


class Classifier:
    def __init__(self, X=None, y=None):
        self.X = X
        self.y = y

    def predict(self, testData):
        pass

    def fit(self):
        pass


class CnnClassifer:
    def __init__(self, fpath=None, epoch=25):
        self.model_file = "cnn.h5"
        self.epoch = epoch
        if fpath:
            self.load_model(fpath)

    def init_model(self):
        # Initialising the CNN
        classifier = Sequential()
        # Adding a first convolutional layer
        classifier.add(Conv2D(32, (3, 3), input_shape = (32, 32, 3), activation = 'relu'))
        classifier.add(MaxPooling2D(pool_size = (2, 2)))
        # Adding a second convolutional layer
        classifier.add(Conv2D(64, (3, 3), activation = 'relu'))
        classifier.add(MaxPooling2D(pool_size = (2, 2)))
        # Step 3 - Flattening
        classifier.add(Flatten())
        # Step 4 - Full connection
        classifier.add(Dense(units = 128, activation = 'relu'))
        classifier.add(Dense(units = 1, activation = 'sigmoid'))
        # Compiling the CNN
        classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

        self.model = classifier

    def load_model(self, fpath):
        m_file = Path(fpath)
        self.init_model()

        if m_file.is_file():
            self.model.load_weights(m_file.as_posix())
        else:
            m_file = Path(
                __file__).resolve().parent / self.model_file
            if m_file.is_file():
                self.model.load_weights(m_file.as_posix())
            else:
                logger.error("{0}, file not found".format(m_file))
                sys.exit(0)

    def dump_model(self, fpath="./"):
        svm_file = Path(fpath) / self.model_file
        self.model.save_weights(svm_file.as_posix())

    def predict(self, X):
        return self.model.predict(X, batch_size=32)

    def fit(self, trainX, trainY, testX, testY):
        self.init_model()
        # construct the training image generator for data augmentation
        aug = ImageDataGenerator(rotation_range=20, zoom_range=0.15,
        	width_shift_range=0.2, height_shift_range=0.2, shear_range=0.15,
        	horizontal_flip=True, fill_mode="nearest")
        self.history = self.model.fit_generator(aug.flow(trainX, trainY, batch_size=32),
    	   validation_data=(testX, testY), steps_per_epoch=len(trainX),
    	      epochs=self.epoch)



class SvmClassifierCV(Classifier):
    """ Opencv svm classifier"""
    def __init__(self, X=None, y=None, fpath=None):
        super(SvmClassifierCV, self).__init__(X=X, y=y)
        self.model_file = "svm_grain-cob.dat"
        if fpath:
            self.load_model(fpath)

    def init_model(self, c=1, gamma=1):
        # opencv SVM
        self.model = cv2.ml.SVM_create()
        self.model.setKernel(cv2.ml.SVM_LINEAR)
        self.model.setType(cv2.ml.SVM_C_SVC)
        self.model.setC(c)
        self.model.setGamma(gamma)

    def dump_model(self, fpath="./"):
        svm_file = Path(fpath) / self.model_file
        self.model.save(svm_file.as_posix())

    def load_model(self, fpath):
        svm_file = Path(fpath)

        if svm_file.is_file():
            self.model = cv2.ml.SVM_load(svm_file.as_posix())
        else:
            svm_file = Path(
                __file__).resolve().parent / self.model_file
            if svm_file.is_file():
                self.model = cv2.ml.SVM_load(svm_file.as_posix())
            else:
                logger.error("{0}, file not found".format(svm_file))
                sys.exit(0)

    def predict(self, X_test, fpath=None):
        if fpath:
            self.load_model(fpath=fpath)
        pred = self.model.predict(np.float32(X_test))[1]
        return np.hstack(pred)

    def fit(self, X_train, y_train):
        # opencv SVM
        self.init_model()
        # train
        self.model.trainAuto(np.float32(X_train), cv2.ml.ROW_SAMPLE, y_train)

    def score(self, X_test, y_true):
        y_pred = self.predict(X_test)
        return accuracy_score(y_true, y_pred)
