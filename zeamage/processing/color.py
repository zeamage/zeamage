import numpy as np

from .display import display


@display
def color_var(img):
    """ Compute color variance between differents image channels.

    The algorithm give a very basic information about image colorfulness.

    Args:
        img (np.ndarray): Image to process
    Returns:
        Black and white image corresponding to colorfulness
    """
    img = img.copy()
    vmeans = np.mean(img[:,:,:2], axis=2)
    img[:,:,0] = vmeans
    img[:,:,1] = vmeans
    p_var = np.var(img, axis=2)
    return np.array(255 * (p_var / np.max(p_var)), np.uint8)
