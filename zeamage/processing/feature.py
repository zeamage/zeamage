import cv2
import skimage as skim
from skimage import feature as skimfeature
import numpy as np



def patch_to_features(patch, patch_mask=None):
    # FEATURES:
    features = []
    num = 0

    # hog
    # hog_feature, hog_num = hog(patch)
    # num += hog_num
    # features.append(hog_feature)

    # local binary pattern features
    lbp_feature, lbp_num = lbp_hist(patch, patch_mask)
    num += lbp_num
    features.append(lbp_feature)

    # color feature
    # colorfulness_feature, colorfulness_num =    rgb_stat_colorfulness(patch, patch_mask)
    # num += colorfulness_num
    # features.append(colorfulness_feature)

    # meanStdDev_feature, meanStdDev_num =    rgb_stat_meanStdDev(patch, patch_mask)
    # num += meanStdDev_num
    # features.append(meanStdDev_feature)

    rgb_feature, rgb_num = rgb_hist(patch, patch_mask)
    num += rgb_num
    features.append(rgb_feature)

    # hsv_feature, hsv_num = hsv_hist(patch, patch_mask)
    # num += hsv_num
    # features.append(hsv_feature)

    # hue_moement
    # hue_feature, hue_num = hu_moments(patch, patch_mask)
    # num += hue_num
    # features.append(hue_feature)

    # haralick
    # haralick_feature, haralick_num = haralick(patch, patch_mask)
    # num += haralick_num
    # features.append(haralick_feature)

    return np.hstack(features), num


def _chan_hist(chan, mask=None, bins=8, range=[0, 256], norm=True):

    if mask is not None:
        chan = chan[mask!=0,...]

    (hist, _) = np.histogram(chan.ravel(),bins,range)

    # normalize the histogram
    hist = hist.astype("float")
    if norm == True:
        hist /= (hist.sum() + 1e-7)

    return hist

def rgb_hist(img, mask=None):
    """ CALCULATE RGB HISTOGRAM. 24 histograms"""
    features = []

    img_rgb = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

    # grab the image channels, initialize the tuple of colors,
    # the figure and the flattened feature vector
    colors = ("R", "G", "B")
    chans = cv2.split(img_rgb)
    # loop over the RGB channels
    for (chan, color) in zip(chans, colors):
    	# create a histogram for the current channel and
    	# concatenate the resulting histograms for each/hist.
        hist = _chan_hist(chan,mask,8,[0, 256],True)
        features.extend(hist)
    return features, 24


def hsv_hist(img, mask=None):
    """ CALCULATE SATURATION HISTOGRAM """
    features = []
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    # grab the image channels, initialize the tuple of colors,
    # the figure and the flattened feature vector
    colors = ("H", "S", "V")
    chans = cv2.split(img_hsv)
    for (chan, color) in zip(chans, colors):
    	# create a histogram for the current channel and
    	# concatenate the resulting histograms for each/hist.
        hist = _chan_hist(chan,mask,8,[0, 256],True)
        features.extend(hist)
    return features, 24


def lbp_hist(image, mask=None, numPoints=24, radius=8):
    """Local Binary pattern normalized histogram

        Compute the Local Binary Pattern representation
        of the image, and then use the LBP representation
        to build the histogram of patterns
    """
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    lbp = skimfeature.local_binary_pattern(image, numPoints, radius, method="uniform")
    hist = _chan_hist(
            lbp,
            mask=mask,
            bins=np.arange(0, numPoints + 3),
            range=(0, numPoints + 2),
            norm=True)
    return hist, numPoints+2

def rgb_stat_meanStdDev(image, mask=None):
    mean, std = cv2.meanStdDev(image, mask)
    std = std.flatten()
    mean = mean.flatten()

    return std, 3


def rgb_stat_colorfulness(image, mask=None):
    # split the image into its respective RGB components, then mask
    # each of the individual RGB channels so we can compute
    # statistics only for the masked region
    (B, G, R) = cv2.split(image.astype("float"))
    R = np.ma.masked_array(R, mask=mask)
    G = np.ma.masked_array(B, mask=mask)
    B = np.ma.masked_array(B, mask=mask)

    # compute rg = R - G
    rg = np.absolute(R - G)

    # compute yb = 0.5 * (R + G) - B
    yb = np.absolute(0.5 * (R + G) - B)

    # compute the mean and standard deviation of both `rg` and `yb`
    (rbMean, rbStd) = (np.mean(rg), np.std(rg))
    (ybMean, ybStd) = (np.mean(yb), np.std(yb))

    # combine the mean and standard deviations
    stdRoot = np.sqrt((rbStd ** 2) + (ybStd ** 2))
    meanRoot = np.sqrt((rbMean ** 2) + (ybMean ** 2))

    # derive the "colorfulness" metric and return it
    colorfulness = stdRoot + (0.3 * meanRoot)
    return colorfulness, 1



def hog(img, bin_n=16):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gx = cv2.Sobel(img, cv2.CV_32F, 1, 0)
    gy = cv2.Sobel(img, cv2.CV_32F, 0, 1)
    mag, ang = cv2.cartToPolar(gx, gy)
    bins = np.int32(bin_n*ang/(2*np.pi))    # quantizing binvalues in (0...16)
    hists = [np.bincount(bins.ravel(), mag.ravel(), bin_n)]
    hist = np.hstack(hists)     # hist is a 64 bit vector
    return hist, bin_n


def hu_moments(img ,msk=None):
    if msk is not None:
        img[msk==0] = 0
    num = 7
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    feature = cv2.HuMoments(cv2.moments(img), num).flatten()
    return feature, num

# import mahotas
#
# def haralick(image, msk):
#     # convert the image to grayscale
#     gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#     # compute the haralick texture feature vector
#     haralick = mahotas.features.haralick(gray).mean(axis=0)
#     # return the result
#     return haralick, 13
